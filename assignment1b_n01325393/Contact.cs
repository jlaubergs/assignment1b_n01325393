﻿using System;

namespace assignment1b_n01325393
{
    public class Contact
    {

        // CONTACT CLASS
        // -- Contact name
        // -- Contact e-mail
        // -- Contact phone number


        private string name;
        private string email;
        private string phone;


        // PROPERTY ACCESSORS

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value.ToLower(); }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

    }
}
