﻿<%@ Page Language="C#" CodeBehind="Default.aspx.cs" Inherits="assignment1b_n01325393.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Digital Project Inquiry for Businesses</title>
    <script runat="server"></script>
</head>
<body>
    <h1>Digital Project Inquiry for Businesses</h1>
    <form id="form1" runat="server">
        <div>
            <h2>Business</h2>
            <asp:ValidationSummary 
                ID="ValidationSummary" 
                runat="server" 
                HeaderText="Following errors have occured....." 
                ShowMessageBox="false" 
                DisplayMode="BulletList" 
                ShowSummary="true"
                Width="450"
                ForeColor="Red"
            />
            <div>
                <label for="companyName">Company Name <span style="color:red;">*</span></label>
                <asp:TextBox runat="server" id="companyName" placeholder="e.g. Smith Industries"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" id="companyNameRequiredValid" controltovalidate="companyName" errormessage="Please enter the name of your company" />
            </div>
            <div>
                <label for="companyIndustry">Industry <span style="color:red;">*</span></label>
                <asp:DropDownList id="companyIndustry" runat="server">
                    <asp:ListItem value="Education"         text="Education" />
                    <asp:ListItem value="Government"        text="Government" />
                    <asp:ListItem value="Healthcare"        text="Healthcare" />
                    <asp:ListItem value="Finance"           text="Finance" />
                    <asp:ListItem value="Manufacturing"     text="Manufacturing" />
                    <asp:ListItem value="Telecom"           text="Telecom" />
                    <asp:ListItem value="Other"             text="Other" Selected="true" />
                </asp:DropDownList>
            </div>
        </div>
       
        <div>
            <h2>Contact Person</h2>
            <div>
                <label for="contactName">Full Name <span style="color:red;">*</span></label>
                <asp:TextBox runat="server" ID="contactName" placeholder="e.g. John Smith"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" id="contactNameRequiredValid" controltovalidate="contactName" errormessage="Please enter your name" />
                <asp:RegularExpressionValidator ID="contactNameRegExValid" runat="server" ValidationExpression="^[a-zA-Z.'-]+(?: +[a-zA-Z.'-]+)+$" ControlToValidate="contactName" ErrorMessage="Enter a valid full name. (Use only latin characters)"></asp:RegularExpressionValidator>
            </div>  
            <div>
                <label for="contactEmail">E-mail <span style="color:red;">*</span></label>
                <asp:TextBox runat="server" ID="contactEmail" placeholder="e.g. john@mail.ca"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" id="contactEmailRequiredValid" controltovalidate="contactEmail" errormessage="Please enter your e-mail" />
                <asp:RegularExpressionValidator ID="contactEmailRegExValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="contactEmail" ErrorMessage="Email is incorrect"></asp:RegularExpressionValidator>
            </div>
            <div>
                <label for="contactPhone">Phone Number <span style="color:red;">*</span></label>
                <span>+</span>
                <asp:TextBox runat="server" ID="contactPhoneCountry" MaxLength="6" placeholder="e.g. 1"></asp:TextBox>
                <asp:TextBox runat="server" ID="contactPhone" placeholder="e.g. 6471234123"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" id="contactPhoneCountryRequiredValid" controltovalidate="contactPhoneCountry" errormessage="Please specify the country code" />
                <asp:RegularExpressionValidator ID="contactPhoneCountryRegExValid" runat="server" ValidationExpression="^[0-9]*$" ControlToValidate="contactPhoneCountry" ErrorMessage="Enter a valid country code"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator runat="server" id="contactPhoneRequiredValid" controltovalidate="contactPhone" errormessage="Please enter your phone number" />
                <asp:RegularExpressionValidator ID="contactPhoneRegExValid" runat="server" ValidationExpression="^[0-9]*$" ControlToValidate="contactPhone" ErrorMessage="Enter a valid phone number"></asp:RegularExpressionValidator>
            </div>
        </div>  
            
        <div>
            <h2>Project</h2>
            <div>
                <label for="projectType">Type <span style="color:red;">*</span></label>
                <asp:RadioButtonList id="projectType" runat="server">
                    <asp:ListItem value="native"    text="Native App" />
                    <asp:ListItem value="cross"     text="Cross Platform App" />
                    <asp:ListItem value="web"       text="Web App" />
                    <asp:ListItem value="other"     text="Other" />
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" id="projectTypeRequiredValid" controltovalidate="projectType" errormessage="Please select the type of a project" />
            </div> 
            <div>
                <label for="projectServices">Services</label>
                <div ID="projectServices" runat="server">
                    <asp:CheckBox runat="server" ID="projectService1" Text="Consultation" />
                    <asp:CheckBox runat="server" ID="projectService2" Text="Design" />
                    <asp:CheckBox runat="server" ID="projectService3" Text="Front-end Development" />
                    <asp:CheckBox runat="server" ID="projectService4" Text="Back-end Development" />
                    <asp:CheckBox runat="server" ID="projectService5" Text="Quality Assurance" />
                </div>
            </div>
            <div>
                <label for="projectBudget">Budget <span style="color:red;">*</span></label>
                <asp:TextBox runat="server" ID="projectBudget" placeholder="e.g. 10000"></asp:TextBox><span>CA$</span>
                <asp:RequiredFieldValidator runat="server" id="projectBudgetRequiredValid" controltovalidate="projectBudget" errormessage="Please enter a possible budget for the project" />
                <asp:RegularExpressionValidator ID="projectBudgetRegExValid" runat="server" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" ControlToValidate="projectBudget" ErrorMessage="Enter a valid amount"></asp:RegularExpressionValidator>
                <asp:CompareValidator runat="server" ID="projectBudgetCompareValid" ControlToValidate="projectBudget" Type="Double" Operator="GreaterThanEqual" ValueToCompare="2000" ErrorMessage="Your budget has to be at least 2000 CA$"></asp:CompareValidator>
            </div>
            <div>
                <label for="projectDeadline">Days until delivery <span style="color:red;">*</span></label>
                <asp:TextBox runat="server" ID="projectDeadline" placeholder="e.g. 90"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" id="projectDeadlineRequiredValid" controltovalidate="projectDeadline" errormessage="Please enter approx. amound of days until project has to be delivered" />
                <asp:RangeValidator runat="server" ID="projectDeadlineRangeValid" ControlToValidate="projectDeadline" Type="Integer" MinimumValue="5" MaximumValue="365" ErrorMessage="Online inquiries can only be made for projects with a deadline from 5-365 days"></asp:RangeValidator>
            </div>
        </div>   
            
        <asp:Button ID="inquirySubmit" Text="Submit" OnClick="Inquiry" runat="server" />
        
        <div runat="server" ID="inquiryResults"></div>
            
    </form>
</body>
</html>