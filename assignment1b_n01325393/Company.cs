﻿using System;

namespace assignment1b_n01325393
{
    public class Company
    {

        // COMPANY CLASS
        // -- Company name
        // -- Industry of the company


        public string name;
        public string industry;

        public Company(string obName, string obIndustry)
        {
            name = obName;
            industry = obIndustry;
        }

    }
}
