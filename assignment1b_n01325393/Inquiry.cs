﻿using System;

namespace assignment1b_n01325393
{
    public class Inquiry
    {
        // INQUIRY CLASS
        // -- Company object
        // -- Contact object
        // -- Project object

        public Company inquiryCompany;
        public Contact inquiryContact;
        public Project inquiryProject;


        // Local variable
        double avgCost;
        double inquiryCost = 50;
     


        public Inquiry(Company obCompany, Contact obContact, Project obProject)
        {
            inquiryCompany = obCompany;
            inquiryContact = obContact;
            inquiryProject = obProject;
        }

        // * Creates a summary of the inquiry
        public string CreateInquiryConfirmation()
        {
            string res = "<div><h2>Thank you!</h2>" +
                "<p>We have received your inquiry and will contact you within the next 3 days</p>" +
                "<h3>Summarised information:</h3><style>.res{text-align: left;}</style><table class='res'>";

            res += "<tr><th>Company</th><td>" + inquiryCompany.name + "</td></tr>";
            res += "<tr><th>Industry</th><td>" + inquiryCompany.industry + "</td></tr>";

            res += "<tr><th colspan='2'>Contact person</th></tr>";
            res += "<tr><th>Full name</th><td>" + inquiryContact.Name + "</td></tr>";
            res += "<tr><th>Phone number</th><td>" + inquiryContact.Phone + "</td></tr>";
            res += "<tr><th>Email</th><td>" + inquiryContact.Email + "</td></tr>";

            res += "<tr><th colspan='2'>Project</th></tr>";
            res += "<tr><th>Type</th><td>" + inquiryProject.typeName + "</td></tr>";
            res += "<tr><th>Services</th><td>" + String.Join(", ", inquiryProject.services.ToArray()) + "</td></tr>";
            res += "<tr><th>Budget</th><td>" + CheckInquiryBudget() + "</td></tr>";
            res += "<tr><th>Average Cost</th><td>" + avgCost + " CA$ (TAX Included)</td></tr>";
            res += "<tr><th>Deadline</th><td>" + CheckInquiryDeadline() + "</td></tr>";

            res += "</table>";

            return res;
        }

        // * Checks the size of the provided budget against the average cost on similar type of projects
        public string CheckInquiryBudget()
        {
            if (inquiryProject.type == "native")
            {
                avgCost = 3000;
            }
            else if (inquiryProject.type == "cross")
            {
                avgCost = 5000;
            }
            else if (inquiryProject.type == "web")
            {
                avgCost = 2000;
            }
            else
            {
                avgCost = 4000;
            }

            foreach (string service in inquiryProject.services){
                if (service == "Consultation")
                {
                    avgCost += 100;
                }
                else if (service == "Design")
                {
                    avgCost += 800;
                }
                else if (service == "Front-end Development")
                {
                    avgCost += 1500;
                }
                else if (service == "Back-end Development")
                {
                    avgCost += 3000;
                }
                else if (service == "Quality Assurance")
                {
                    avgCost += 500;
                }
            }

            avgCost = (avgCost + inquiryCost) * 1.13;
            string res;

            if(inquiryProject.budget < avgCost)
            {
                res = "<span style='color:red;'>" + inquiryProject.budget + " CA$ (Smaller than average)</span>";
            } 
            else 
            {
                res = "<span style='color:green;'>" + inquiryProject.budget + " CA$ (Good budget according to similar projects)</span>";
            }

            return res;
        }

        // * Checks the lenght of deadline
        public string CheckInquiryDeadline()
        {
            string res;

            if (inquiryProject.deadline < 15)
            {
                res = "<span style='color:red;'>" + inquiryProject.deadline + " days (Shorter than average)</span>";
            }
            else if (inquiryProject.deadline <= 30)
            {
                res = "<span style='color:orange;'>" + inquiryProject.deadline + " days</span>";
            }
            else 
            {
                res = "<span style='color:green;'>" + inquiryProject.deadline + " days</span>";
            }

            return res;
        }
    }
}
