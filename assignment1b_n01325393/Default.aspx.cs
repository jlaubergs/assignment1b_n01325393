﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;

namespace assignment1b_n01325393
{

    public partial class Default : System.Web.UI.Page
    {
        public void Inquiry(object sender, EventArgs args)
        {


            // DEFINING COMPANY
            string company_name = companyName.Text.ToString();
            string company_industry = companyIndustry.SelectedItem.Value.ToString();

            Company inquiryCompany = new Company(company_name, company_industry);


            // DEFINING CONTACT
            string contact_name = contactName.Text.ToString();
            string contact_phone = "+" + contactPhoneCountry.Text.ToString() + contactPhone.Text.ToString();
            string contact_email = contactEmail.Text.ToString();

            Contact inquiryContact = new Contact();
            inquiryContact.Name = contact_name;
            inquiryContact.Phone = contact_phone;
            inquiryContact.Email = contact_email;


            // DEFINING PROJECT
            string project_type = projectType.SelectedItem.Value.ToString();

            // * Code taken and adjusted from in-class example
            List<string> project_services = new List<string>();
            foreach (Control control in projectServices.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox service = (CheckBox)control;
                    if (service.Checked)
                    {
                        project_services.Add(service.Text);
                    }
                }
            }
            float project_budget = int.Parse(projectBudget.Text);
            int project_deadline = int.Parse(projectDeadline.Text);


            Project inquiryProject = new Project(project_type, project_services, project_budget, project_deadline);


            // DEFINING INQUIRY

            Inquiry submitedInquiry = new Inquiry(inquiryCompany, inquiryContact, inquiryProject);

            inquiryResults.InnerHtml = submitedInquiry.CreateInquiryConfirmation();
            //inquiryResults.InnerHtml += submitedInquiry.Test();

        }
    }
}
