﻿using System;
using System.Collections.Generic;

namespace assignment1b_n01325393
{
    public class Project
    {

        // PROJECT CLASS
        // -- Project type
        // -- Project services
        // -- Project budget
        // -- Project deadline


        public string type;
        public string typeName;
        public List<string> services;
        public float budget;
        public int deadline;

        public Project(string obType, List<string> obServices, float obBudget, int obDeadline)
        {
            type = obType;
            services = obServices;
            budget = obBudget;
            deadline = obDeadline;

            if(obType == "native")
            {
                typeName = "Native App";
            }
            else if(obType == "cross")
            {
                typeName = "Cross Platform App";
            } 
            else if (obType == "web")
            {
                typeName = "Web App";
            } 
            else 
            {
                typeName = "Other";
            }


        }

    }
}
